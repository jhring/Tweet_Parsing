# Tweet Parsing

This will ultimately be a collection of programs for high performance processing
of tweets. Currently the only implemented program gathers all tweets that mention
a specified user.

## Build Instructions
The requirements for this project are recent versions of GCC, Boost, and CMake.
To build simply issue the following commands.
```console
git clone https://gitlab.com/jhring/Tweet_Parsing.git 
cd Tweet_Parsing
cmake -DCMAKE_BUILD_TYPE=Release .. && make
```
The binaries will be located in the build directory. 

### On the VACC
The VACC's build system is quite dated and lacks boost.
Fortunately it's easy to grab the required programs and libraries.
Note that all changes made to the user profile with spack are temporary.
```console
spack load gcc
spack load boost
spack load cmake
```

## Using the Program(s)
Running the programs in this repository should be quite simple.
All programs support the use of command line arguments and some may support
an optional configuration file.

To run a program with a configuration file:

`./<program> -c <configFile>`

Arguments specified in the config file may be overwritten by passing the argument
on the command line. For example:

` ./<program> -c <configFile> --<option> <value>`

To see a list of all available arguments and their descriptions run:

` ./<program> --help`