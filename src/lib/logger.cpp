/*! \file Logger.cpp
 *  \brief Advanced boost logging implementation
 *
 *  \author Johnathan Herbst
 *  \author John H. RIng IV
 *  \date July 13th, 2016
 */

#include "logger.h"

#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/formatter_parser.hpp>

#include <boost/iostreams/stream.hpp>

#include <boost/filesystem.hpp>

#include <memory>
#include <sstream>
#include <fstream>

namespace fs = boost::filesystem;
namespace lg = boost::log;
namespace io = boost::iostreams;

RotatingFileSink::RotatingFileSink(const std::string& logPath,
                                   std::uintmax_t fileSize, size_t maxFiles):
        m_logPath(logPath),
        m_fileSize(fileSize),
        m_maxFiles(maxFiles)
{
    boost::system::error_code error;
    fs::create_directories(fs::path(m_logPath).parent_path(), error);
}

std::streamsize RotatingFileSink::write(const char* s, std::streamsize n)
{
    boost::system::error_code error;
    if(fs::file_size(m_logPath, error) + n >= m_fileSize && !error)
        rotate(0);

    std::ofstream file(m_logPath, std::ios_base::out | std::ios_base::app);
    file.write(s, n);
    return n;
}

void RotatingFileSink::rotate(size_t i)
{
    std::stringstream rotatePath;
    rotatePath << m_logPath;
    if(i > 0)
        rotatePath << "." << i;

    if(fs::exists(rotatePath.str()))
    {
        rotate(i + 1);

        if(i + 1 >= m_maxFiles)
        {
            fs::remove(rotatePath.str());
        }
        else
        {
            std::stringstream nextRotatePath;
            nextRotatePath << m_logPath << "." << i + 1;
            fs::rename(rotatePath.str(), nextRotatePath.str());
        }
    }
}

BOOST_LOG_GLOBAL_LOGGER_INIT(logger, Logger)
{
return Logger(lg::keywords::channel = "app",
        lg::keywords::severity = info);
}

BOOST_LOG_ATTRIBUTE_KEYWORD(channel, "Channel", std::string)
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", LogLevel)

lg::formatting_ostream& operator<< (lg::formatting_ostream& strm,
                                    const lg::to_log_manip<LogLevel, tag::severity>& manip)
{
    static const char* const str[] =
            {
                    "DEBUG",
                    "INFO",
                    "WARNING",
                    "ERROR",
                    "CRITICAL"
            };

    LogLevel lvl = manip.get();
    if (static_cast< std::size_t >(lvl) < (sizeof(str) / sizeof(*str)))
        strm << str[lvl];
    else
        strm << static_cast< int >(lvl);
    return strm;
}

void setupFileLogger(const std::string& logDir, const std::string& logName,
                     LogLevel level)
{
    lg::register_simple_formatter_factory<LogLevel, char>(std::string("Severity"));

    auto core = lg::core::get();

    core->add_global_attribute("TimeStamp", lg::attributes::local_clock());

    auto backend = boost::make_shared<lg::sinks::text_ostream_backend>();
    typedef io::stream<RotatingFileSink> RotatingFileStream;
    backend->add_stream(boost::make_shared<RotatingFileStream>(
            (fs::path(logDir) / logName).string(), 100 * 1024, 5));
    backend->auto_flush(true);

    typedef lg::sinks::synchronous_sink<lg::sinks::text_ostream_backend> Sink;
    auto sink = boost::make_shared<Sink>(backend);

    sink->set_formatter(
            lg::expressions::stream
                    << lg::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S,%f")
                    << " - " << channel << " - " << severity << " - " << lg::expressions::smessage
    );

    core->add_sink(sink);

    core->set_filter(
            severity >= level
    );

    lg::add_common_attributes();
}
