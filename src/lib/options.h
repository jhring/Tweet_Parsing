/*! \file options.h
 *  \brief A way to read program options using
 *      the command line or a config file.
 *
 *  This class creates a simple and unifed way for one to
 *  parse program options from a variety of sources. It
 *  allows required options to be selected from either
 *  source, a feature not currently implemented in boost.
 *
 *  Usage:
 *  Write a class that inherits from Options, adding in
 *  application specific options. Use this sub class to
 *  parse arguments from the command line or config file.
 *
 *
 *  \author Johnathan Herbst
 *  \author John H. RIng IV
 *  \date July 13th, 2016
 */


#ifndef TWEET_PARSING_OPTIONS_H
#define TWEET_PARSING_OPTIONS_H

#include <boost/program_options.hpp>

#include <string>
#include <iostream>
#include <fstream>
#include <exception>
#include <list>
#include <map>

using std::string;
namespace po = boost::program_options;

namespace Options
{

    typedef po::error OptionsError;
    typedef po::required_option Required;

    typedef po::variable_value ValueType;
    typedef std::map<string, ValueType> VariableMap;

//! Thrown when help option is enabled.
    struct Help: OptionsError
    {
        Help():
                OptionsError("Help flag is set.")
        {}
    };

//! Thrown when version option is enabled.
    struct Version: OptionsError
    {
        Version():
                OptionsError("Version flag is set.")
        {}
    };

//! Thrown when value is correct type but
//! fails other requirments
    struct Invalid: OptionsError
    {
        Invalid(const std::string& s):
                OptionsError(s)
        {}
    };

/*!
 * \brief Store and parse options from the command
 *    line and a config file.
 */
    class Options
    {
    public:
        Options(bool configRequired=false):
                m_commandLineOptions("Command Line Options"),
                m_configOptions("Config File Options"),
                m_configRequired(configRequired)
        {
            m_commandLineOptions.add_options()
                    ("help,h", "Print help message")
                    ("version,v", "Print Program Version")
                    ("config,c", po::value<string>(), "Configuration file to augment options")
                    ;
        }

        virtual ~Options() {}

        /*!
         * \brief Parse command line options.
         *
         * \throw Help		When help option is specified.
         * \throw Version	When version option is specified.
         * \throw Required	When a required option or constraint is not satisfied.
         *
         * \param[in]	argc	Number of arguments in the argument vector.
         * \param[in]	argv	Argument vector.
         */
        void ParseCL(int argc, char* argv[])
        {
            po::variables_map vm;
            po::store(po::parse_command_line(argc, argv, m_commandLineOptions), vm);
            if(!vm["help"].empty()) { throw Help(); }
            if(!vm["version"].empty()) { throw Version(); }
            if(!vm["config"].empty())
            {
                std::cout << "File: " << vm["config"].as<string>() << std::endl;
                std::ifstream configFile(vm["config"].as<string>().c_str());
                po::store(po::parse_config_file(configFile, m_configOptions, true), vm);
            }
            else if(m_configRequired)
            {
                throw Required("config");
            }
            Notify(vm);
        }

        //! Write a string representation to the specified output stream
        friend std::ostream& operator<<(std::ostream& os, const Options& obj);

        const ValueType& operator[](const std::string& key)
        {
            return m_vm[key];
        }

    protected:
        //! Hold a constraint between any number of options to use one of the options values.
        struct Constraint
        {
            typedef std::list<string> Keys;
            string name;
            bool required;
            Keys keys;

            /**
             * \brief Constructor
             *
             * \param[in]	name		The name to use for the compiled option.
             * \param[in]	required	Whether the constraint is required.
             */
            Constraint(string name, bool required=false):
                    name(name),
                    required(required)
            {}

            /**
             * \brief Add an option to the constraint.
             *
             * \param[in]	key	The key of the option to add.
             * \return this so the method can be chained.
             */
            Constraint& operator()(string key)
            {
                keys.push_back(key);
                return *this;
            }
        };
        typedef std::list<Constraint> Constraints;

        Constraints m_constraints;
        VariableMap m_vm;
        po::options_description m_commandLineOptions;
        po::options_description m_configOptions;
        bool m_configRequired;

    private:
        /**
         * \biref Parse and validate all the options and constraints.
         *
         * \throw Required	When a required option or constraint is not satisfied.
         *
         * \param[in]	vm	The variables map to validate and use to satisfy constraints.
         */
        void Notify(po::variables_map& vm)
        {
            po::notify(vm);
            m_vm.insert(vm.begin(), vm.end());
            for(Constraints::iterator iConstraint = m_constraints.begin();
                iConstraint != m_constraints.end(); iConstraint++)
            {
                po::variable_value val;
                for(Constraint::Keys::iterator iKey = iConstraint->keys.begin();
                    iKey != iConstraint->keys.end(); iKey++)
                {
                    val = m_vm[*iKey];
                    if(!val.empty())
                    {
                        break;
                    }
                }
                if(!val.empty())
                {
                    m_vm[iConstraint->name] = val;
                }
                else if(iConstraint->required)
                {
                    throw Required(iConstraint->name);
                }
            }
        }}; // class Options

    inline std::ostream& operator<<(std::ostream& os, const Options& obj)
    {
        os << obj.m_commandLineOptions << obj.m_configOptions;
        return os;
    }

} // namespace Options


#endif //TWEET_PARSING_OPTIONS_H
