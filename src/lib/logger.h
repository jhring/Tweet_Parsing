/*! \file logger.h
 *  \brief Advanced boost logging implementation
 *
 *  \author Johnathan Herbst
 *  \author John H. RIng IV
 *  \date July 13th, 2016
 */

#ifndef TWEET_PARSING_LOGGER_H
#define TWEET_PARSING_LOGGER_H

#define BOOST_LOG_DYN_LINK 1

#include <boost/log/trivial.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/severity_channel_logger.hpp>

#include <boost/iostreams/categories.hpp>

#include <string>

class RotatingFileSink
{
public:
    typedef char char_type;
    typedef boost::iostreams::sink_tag category;

    RotatingFileSink(const std::string& logPath, uintmax_t fileSize,
                     size_t maxFiles);

    std::streamsize write(const char* s, std::streamsize n);

private:
    void rotate(size_t i);

    std::string m_logPath;
    uintmax_t m_fileSize;
    size_t m_maxFiles;
};

enum LogLevel
{
    debug,
    info,
    warning,
    error,
    critical
};

typedef boost::log::sources::severity_channel_logger<LogLevel> Logger;

BOOST_LOG_GLOBAL_LOGGER(logger, Logger)

extern void setupFileLogger(const std::string& logDir,
                            const std::string& logName, LogLevel level);

#endif //TWEET_PARSING_LOGGER_H
