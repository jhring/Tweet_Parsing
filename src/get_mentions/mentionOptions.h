//
// Created by jhring on 9/2/18.
//

#ifndef TWEET_PARSING_MENTIONOPTIONS_H
#define TWEET_PARSING_MENTIONOPTIONS_H

#include "../lib/options.h"

using namespace std;

class MentionOptions : public Options::Options {
public:
    MentionOptions() {
        m_commandLineOptions.add_options()
                ("inFile,f", po::value<string>(), "Compressed tweet file")
                ("outDir,o", po::value<string>(), "Output directory")
                ("target,t", po::value<int>(), "Twitter ID of target")
                ("logDir,d", po::value<std::string>(), "Log directory")
                ("logName,l", po::value<std::string>(), "Log name")
                ;

        m_configOptions.add_options()
                ("MentionOptions.inFile", po::value<string>(), "Compressed tweet file")
                ("MentionOptions.outDir", po::value<string>(), "Output directory")
                ("MentionOptions.target", po::value<int>(), "Twitter ID of target")
                ("General.logPath", po::value<std::string>(), "Log directory")
                ("MentionOptions.logName", po::value<std::string>(), "Log name")
                ;

        m_constraints.push_back(Constraint("logDir", true)("logDir")("General.logPath"));
        m_constraints.push_back(Constraint("inFile", true)("inFile")("MentionOptions.inFile"));
        m_constraints.push_back(Constraint("target", true)("target")("MentionOptions.target"));
        m_constraints.push_back(Constraint("outDir", true)("outDir")("MentionOptions.outDir"));
        m_constraints.push_back(Constraint("logName", true)("logName")("MentionOptions.logName"));

    }


};

#endif //TWEET_PARSING_MENTIONOPTIONS_H
