#include <string>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/timer.hpp>

#include "mentionOptions.h"
#include "../lib/logger.h"

const static std::string VERSION("0.0.1");

using namespace std;
using namespace boost::filesystem;
namespace pt = boost::property_tree;

void get_mentions(string inFile, string outDir, int target) {
    boost::filesystem::create_directories(outDir);
    string fname = path(inFile).filename().string();

    // load and decompress the file
    auto compFile = boost::iostreams::file_source(inFile);
    boost::iostreams::filtering_istream filter;
    filter.push (boost::iostreams::gzip_decompressor());
    filter.push(compFile);

    // each line is a tweet, we read the line and parse the json
    string itReadLine;
    stringstream ss;
    stringstream ssOut;
    pt::ptree root;
    while (getline (filter, itReadLine)) {
        if (itReadLine.length() > 1) { // skip "\r" etc
            ss << itReadLine;
            pt::read_json(ss, root);
            try {
                for (auto& it : root.get_child("twitter_entities.user_mentions")) {
                    auto id = it.second.get_optional<int>("id");
                    if (id && *id == target) {
                        ssOut << itReadLine << "\n";
                    }
                }
            } catch (const pt::ptree_error &e) {
                BOOST_LOG_CHANNEL_SEV(logger::get(), "app", info)
                    << "skipping: " << itReadLine;
            }

        }
    }

    // write the file
    boost::iostreams::filtering_ostream out;
    out.push( boost::iostreams::gzip_compressor() );
    out.push( boost::iostreams::file_sink(outDir + fname));
    out << ssOut.str().c_str();
}

int main(int argc, char** argv) {
    MentionOptions options;
    try {
        options.ParseCL(argc, argv);
        setupFileLogger(options["logDir"].as<std::string>(),
                        options["logName"].as<std::string>(), debug);
        BOOST_LOG_CHANNEL_SEV(logger::get(), "app", info) << "starting";

        try {
            string inFile = options["inFile"].as<string>();
            string outDir = options["outDir"].as<string>();
            int target = options["target"].as<int>();

            boost::timer t;
            get_mentions(inFile, outDir, target);
            BOOST_LOG_CHANNEL_SEV(logger::get(), "app", info)
                << "duration (seconds): " << t.elapsed();
        } catch (const std::exception &e) { // critical runtime error
            BOOST_LOG_CHANNEL_SEV(logger::get(), "app", critical)
                << "exception in main, stopping: " << e.what();
        }
    }
    catch (Options::Help &e) // user requested help info
    {
        cout << "This program gathers all tweets mentioning a specified user." << "\n\n";
        std::cout << options << std::endl;
    }
    catch (Options::Version &e) // user requested version info
    {
        std::cout << "Version " << VERSION << std::endl;
    }
    catch (Options::Required &e) // user did not specify a required option
    {
        std::cout << e.get_option_name() << " Is a required option" << std::endl;
    }
    catch (Options::Invalid &e)
    {
        std::cout << e.what() << std::endl;
    }
    catch (po::validation_error& e)
    {
        std::cout << e.what() << std::endl;
    }
}